let limit = 5, offset = 0;
let tags = undefined;
document.addEventListener("DOMContentLoaded", function (event) {

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    let editor = new EasyMDE({
        forceSync: true,
        initialValue: '',
        placeholder: 'Détaillez votre question ici!',
        maxHeight: "300px",
//        previewClass: ['editor-preview-font'],
        indentWithTabs: false,
        lineNumbers: true,
        imagePathAbsolute: true,
        uploadImage: true,
        imageUploadFunction: uploadImage,
        previewImagesInEditor: true,
        sideBySideFullscreen: true,
        imageMaxSize: 1024 * 1024 * 30,
        imageUploadEndpoint: "api/v1/image/upload",
        renderingConfig: {
            codeSyntaxHighlighting: true
        }
    });

    /*
     * Gestion de la coloration syntaxique
     */
    hljs.highlightAll();

    // Handling forms
    document.getElementById("simple-editor")
            .addEventListener("submit", handleForm);
    document.getElementById("advanced-editor")
            .addEventListener("submit", handleForm);
    tags = getTags();
    /**
     * Initialisation request
     * @type XMLHttpRequest
     */
    const xhr = new XMLHttpRequest();
    console.log(`api/v1/questions?limit=${limit}&offset=${offset}&${initSearch(tags)}`);
    xhr.open("GET", `api/v1/questions?limit=${limit}&offset=${offset}&${initSearch(tags)}`, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            let board = document.querySelector(".faq-accord");
            let content = "";
            for (let q of JSON.parse(xhr.responseText)) {
                if (parseInt(q.id) > offset)
                    offset = parseInt(q.id);
                content += `
                    <div class="acc">
                        <button class="accordion"><span class="qTitle">${q.title}</span></button>
                        <div class="panel">
                            <div class="content">
                                ${q.body}
                                <br>
                                <hr>
                                <br>
                                <h5 style="text-decoration: underline; font-style: italic;"> Reponse </h5>
                                ${q.answer}
                                <div class="tags">${q.tags}</div>
                            </div>
                        </div>
                    </div>
                    `;
            }
            console.log(offset);
            offset += 1;
            board.classList.remove('center');
            content += `<button onclick="loadMore(event)" class="btn" style="margin: auto;">Charger plus</button>`;
            board.innerHTML = content;
            accordeons();
            hljs.highlightAll();
            console.log("Request done!");
        } else
            console.log("Something is going!")
    };
    xhr.send();
});

function typeQuestion(evt, cityName) {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}


function handleForm(event) {
    event.preventDefault();
    console.table(event);
    if (event.target.id === 'simple-editor') {
        let title = document.getElementById('question').value;
        let email = document.getElementById('mymail').value;
        submitForm(title, email);
    } else if (event.target.id === 'advanced-editor') {
        let title = document.getElementById('question2').value;
        let email = document.getElementById('mymail2').value;
        let body = document.getElementById('body').value;
        submitForm2(title, email, body);
    }
}

function submitForm(title, email) {
    let requestData = {
        title: title,
        email: email,
    };
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "api/v1/questions", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            alert('Votre message a bien été soumis');
            console.log(xhr.responseText);
        }
    };
    console.log(`data=${JSON.stringify(requestData)}`);
    xhr.send(`data=${JSON.stringify(requestData)}`);

}

function submitForm2(title, email, body) {
    console.log("Tobi");
    let requestData = {
        title: title,
        email: email,
        body: body,
    };
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "api/v1/questions", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            alert('Votre message a bien été soumis');
            console.log(xhr.responseText);
        }
    };
    console.log(`data=${JSON.stringify(requestData)}`);
    xhr.send(`data=${JSON.stringify(requestData)}`);
}

function accordeons() {
    let acc = document.getElementsByClassName("accordion");

    for (let i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            let panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
}

function loadMore(event) {
    event.target.remove();
    document.querySelector(".faq-accord").innerHTML += `<div id="loader" style="min-height: 80px;">
<div class="ellipsis"><div></div><div></div><div></div><div></div></div>
    </div>
`;
    const xhr = new XMLHttpRequest();
    console.log(`api/v1/questions?limit=${limit}&offset=${offset}&${initSearch(tags)}`);
    xhr.open("GET", `api/v1/questions?limit=${limit}&offset=${offset}&${initSearch(tags)}`, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            let board = document.querySelector(".faq-accord");
            let content = "";
            if (JSON.parse(xhr.responseText).length === 0) {
                document.querySelector('#loader').remove();
                accordeons();
                hljs.highlightAll();
                return;
            }
            for (let q of JSON.parse(xhr.responseText)) {
                if (parseInt(q.id) > offset)
                    offset = parseInt(q.id);
                content += `
                    <div class="acc">
                        <button class="accordion"><span class="qTitle">${q.title}</span></button>
                        <div class="panel">
                            <div class="content">
                                ${q.body}
                                <br>
                                <hr>
                                <br>
                                <h5 style="text-decoration: underline; font-style: italic;"> Reponse </h5>
                                ${q.answer}
                                <div class="tags">${q.tags}</div>
                            </div>
                        </div>
                    </div>
                    `;
            }
            console.log(offset);
            offset += 1;
            board.classList.remove('center');
            content += `<button onclick="loadMore(event)" class="btn" style="margin: auto;">Charger plus</button>`;
            document.querySelector('#loader').remove();
            board.innerHTML += content;
            accordeons();
            hljs.highlightAll();
            console.log("Request done!");
        } else
            console.log("Something is going!")
    };
    xhr.send();
}


function getTags() {
    let query = window.location.search.replace("?", "").split("&");
    let result = [];
    for (let p of query) {
        if (p.split("=")[0] === 'tag') {
            result.push(p);
        }
    }
    console.log(result);
    return result;
}

function initSearch(tags) {
    if (tags.length == 0)
        return '';
    return tags.reduce((search, param) => {
        return `${search}&${param}`;
    });
}


function search(event) {
    event.preventDefault();
    let value = document.querySelector('#search-tag').value;
    let tags = value.split(/\s+|,\s?/);
    console.log(tags);
    let search = tags.reduce((string, tag, index) => {
        return `${string}${(index !== 0) ? '&' : ''}tag=${tag}`;
    }, "");
    console.log(search);
    let url = `${location.origin + location.pathname}?${search}`;
    console.log(url);
    window.location.assign(url);
}

function uploadImage(file, onSuccess, onError) {
    console.log(file);
    let base64 = toBase64(file)
            .then(base64 => {
                const xhr = new XMLHttpRequest();
                xhr.open("POST", "api/v1/images/upload", true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                        alert('Votre message a bien été soumis');
                        console.log(JSON.parse(xhr.responseText)["data"]["display_url"]);
                        onSuccess(JSON.parse(xhr.responseText)["data"]["display_url"]);
                        onError("Success");
                    }
                };
                console.log(`data=${base64}`);
                xhr.send(`image=${base64}`);
            })
            .catch(error => onError(error));
}

function toBase64(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result.replace(/^.*,/, ''));
        reader.onerror = error => reject(error);
    });
}