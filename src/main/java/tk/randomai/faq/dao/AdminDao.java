package tk.randomai.faq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tk.randomai.faq.beans.Admin;
import tk.randomai.faq.dao.exceptions.DaoException;
import tk.randomai.faq.dao.interfaces.AdminDaoInterface;

public class AdminDao implements AdminDaoInterface {

    public static final String FIND_ADMIN_SQL_QUERY = "SELECT * FROM admins WHERE"
            + " email = ?";
    private static final String INSERT_NEW_ADMIN_SQL = "INSERT INTO admins(email,"
            + " _hash, lname, fname, created_by) VALUES "
            + "(?, ?, ?, ?, ?)";

    @PersistenceContext
    private EntityManager em;

    public AdminDao() {
    }

    @Override
    public Admin findbyEmail(String email) {
        return em.find(Admin.class, em.createNamedQuery("findByEmail")
                .setParameter("adminEmail", email)
                .getFirstResult());
    }

    public void store(Admin admin) {
        if (admin == null) {
            return;
        }
        try {
            em.getTransaction().begin();
            em.persist(admin);
            em.getTransaction().commit();
        }
        catch (Exception e) {
            em.getTransaction().rollback();
            throw new DaoException(e);
        }
    }
}
