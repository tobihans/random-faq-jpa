/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.dao.interfaces;

import tk.randomai.faq.beans.Admin;

public interface AdminDaoInterface {
    Admin findbyEmail(String email);
}
