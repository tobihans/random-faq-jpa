/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tk.randomai.faq.beans.Tag;
import tk.randomai.faq.dao.exceptions.DaoException;

/**
 *
 * @author tobi
 */
public class TagDao {

    private static final String INSERT_TAG = "INSERT INTO tags(tag) VALUES (?)";
    private static final String FIND_TAG = "SELECT tag FROM tags WHERE tag = ?";
    private DaoFactory factory;

    @PersistenceContext
    private EntityManager em;
    
    public TagDao(DaoFactory factory) {
        this.factory = factory;
    }

    public void addTag(Tag tag) {
        if (getTag(tag.getTag()) == null) {
            try {
                
            em.getTransaction().begin();
            em.persist(tag);
            em.getTransaction().commit();
            }
            catch (Exception e) {
                em.getTransaction().rollback();
                throw new DaoException(e);
            }
        }
    }

    public Tag getTag(String tag) {
        return em.find(Tag.class, em.createNamedQuery("find_by_slug")
        .getFirstResult());
    }
}
