/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tk.randomai.faq.beans.Admin;
import tk.randomai.faq.dao.AdminDao;
import tk.randomai.faq.dao.DaoFactory;
import tk.randomai.faq.util.Security;

/**
 *
 * @author tobi
 */
@WebServlet(name = "Administration", urlPatterns = {"/management/administration"})
public class Administration extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Administration</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Administration at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Verification de la connexion active de l'admin
        String from = request.getRequestURL().toString();
        String _context = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                || "https".equals(request.getScheme())
                && request.getServerPort() == 443 ? "" : ":"
                + request.getServerPort())
                + request.getContextPath();
        if (request.getSession().getAttribute("user") == null) {
            String url = _context + "/management/login?from="
                    + URLEncoder.encode(from);
            response.sendRedirect(url);
        }
        getServletContext().getRequestDispatcher("/WEB-INF/addAdmin.jsp")
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("submit") != null) {
            String email = request.getParameter("email");
            String lname = request.getParameter("lname");
            String fname = request.getParameter("fname");
            String pwd1 =request.getParameter("pwd1");
            String pwd2 =request.getParameter("pwd2");
            if (!pwd1.equals(pwd2)) {
                request.setAttribute("notification", "Les deux mots de passe doivent correspondre.");
                getServletContext().getRequestDispatcher("/WEB-INF/addAdmin.jsp")
                        .forward(request, response);
            }
            AdminDao aDao = ((DaoFactory) getServletContext().getAttribute("dao_factory"))
                    .getAdminDao();
            Admin admin = aDao.findbyEmail(email);
            if (admin != null) {
                request.setAttribute("notification", "Cet email existe deja dans la base de donnees.");
                getServletContext().getRequestDispatcher("/WEB-INF/addAdmin.jsp")
                        .forward(request, response);
                return;
            }
            admin = new Admin();
            admin.setLname(lname);
            admin.setFname(fname);
            admin.setEmail(email);
            admin.setHash(Security.hash(pwd1).get("hash"));
//            admin.setCreated_by(
//                    ((Admin) request.getSession()
//                    .getAttribute("user"))
//                    .getId()
//            );
            aDao.store(admin);
            request.setAttribute("notification", "Administrateur ajoute avec succes.");
                getServletContext().getRequestDispatcher("/WEB-INF/addAdmin.jsp")
                        .forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
