/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author kadou
 */
@Entity
@Table(name = "admins")
@NamedQueries({
    @NamedQuery(
    name = "findByEmail",
    query = "SELECT a FROM admins a WHERE a.email = :adminEmail"
    )
})
public class Admin implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "_hash")
    private String hash;
    
    @Column(name ="lname")
    private String lname;
    
    @Column(name ="fname")
    private String fname;
    
    @Column(name ="created_at")
    private String created_at;
    
    @Column(name ="updated_at")
    private String updated_at;
    
    @ManyToOne
    @JoinColumn(name ="id")
    private Admin created_by;
    
    @ManyToOne
    @JoinColumn(name ="id")
    private Admin updated_by;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Admin getCreated_by() {
        return created_by;
    }

    public void setCreated_by(Admin created_by) {
        this.created_by = created_by;
    }

    public Admin getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(Admin updated_by) {
        this.updated_by = updated_by;
    }
}
