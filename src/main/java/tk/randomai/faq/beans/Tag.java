/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author kadou
 */
@Entity
@Table(name = "tags")
@NamedQuery(
    name = "find_by_slug",
    query = "SELECT tag FROM tags t WHERE t.tag = :tagg"
)
public class Tag implements Serializable {
    
    
    @Id
    @GeneratedValue()
    private int id;
    
    @Column(name = "tag")
    private String tag;
    
    @Column(name = "created_at")
    private String created_at;

    @ManyToMany
    @JoinTable(name="questions_tags",
        joinColumns = @JoinColumn(name = "tag"), 
        inverseJoinColumns = @JoinColumn(name = "question"))
    private List<Question> questions;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
