package tk.randomai.faq.util;

import com.password4j.Hash;
import com.password4j.Password;
import java.util.HashMap;

public class Security {
    
    public static HashMap<String, String> hash(String plain_text) {
        HashMap<String, String> result = new HashMap<>();
        Hash hash = Password.hash(plain_text)
                .withBCrypt();
        result.put("hash", hash.getResult());
        return result;
    }
    
    public static boolean check(String pwString, String hash) {
            boolean check;
            check = Password.check(pwString, hash)
                    .withBCrypt();
        return check;
    }
}
